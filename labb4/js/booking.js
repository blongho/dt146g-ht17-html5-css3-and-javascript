/**
 * @file booking.js
 * @author Bernard Che Longho
 * lobe1602
 * <lobe1602@student.miun.se>
 *
 * @description Javascript stuff specific for the booking site <br>
 *  This file contains functions for the following: <br>
 *  - Keep and maintain session storage <br>
 *  - Keep track of bookings (seats) <br>
 *  - Print boarding pass after booking <br>
 *
 * Created    : 2017-12-14 <br>
 * Last edited: 2017-12-20 <br>
 */
/** the seats for the site **/
var seats = [];

/** Number of rows for the seats */
const ROWS = 6;

/** Number of columns for the seats */
const COLUMNS = 3;

/**
 * Default for seat sizes. Change here if seat sizes have to change
 *
 * Desired seat size
 */
const IMG_WIDTH = 40;
const IMG_HEIGHT = 40;

var firstName = "";  // passenger first name
var lastName = "";   // passenger last name
var departure = "";  // departure station
var destination = "";// arrival station
var depDate = "";    // departure date
var arrDate = "";    // arrival date
var status = "";     // kind of reserved seat {economy|business}
var seatNr = "";     // seat number reserved
//var personnr = "";    // the social security number of the passenger

/**
 * Set some style for an image <br>
 *
 * Business class seats are larger than economy class seats. <br>
 * This is achieved by the size of the border radius <br>
 *
 * @param seat the image whose styles have to be updated <br>
 * @param border the size of the border radius <br>
 */
function styleSeat(seat, border)
{
	seat.style.border = "1px solid grey";
	seat.style.borderRadius = border;
	seat.style.margin = "0.1em";
}

/**
 * Load the seats for the plane
 *
 * A seat is an image object.
 * Seat: an transparent 40*40 image <br>
 * Seat number:  the alt attribute, automatically generated 1A, 1B, 1C, 2A,...6C
 * Seat type{economy|business}: the image title
 * Seat size: based on type, border-radius value is bigger or smaller
 * Seat color: beginning, everything is green.
 *
 */
function loadSeats()
{
	for (var i = 1; i <= ROWS; i++) {
		for (var j = 0; j < COLUMNS; j++) {

			var seat = new Image();

			seat.title = i < 3 ? "business" : "economy";

			seat.setAttribute("class", "seat");

			seat.alt = i + String.fromCharCode(65 + j);

			var imgBorderRadius = seat.title === "business" ?
				"0.3em" : "0.8em";

			seat.src = "img/seat.png";
			seat.width = IMG_WIDTH;
			seat.height = IMG_HEIGHT;
			setSeatBackground(seat, "green");
			styleSeat(seat, imgBorderRadius);

			seats.push(seat);
		}
	}
}

/**
 * Show the booking seats when page loads
 * This builds a table like this <br>
 *  &nbsp;&nbsp;  A   B   C <br>
 *  1  img img  img <br>
 *  2  img img  img <br>
 *  ... <br>
 *  6  img img  img <br>
 *
 *  img is an attribute of the object seat which is an img element (total 18 of
 *  them) of the <code>seats</code> array.
 */
function showSeatTable()
{
	var table = document.createElement("table");
	table.id = "seatView";
	var thead = document.createElement("thead");
	var header = document.createElement("tr");
	header.appendChild(document.createElement("th"));

	// make the headers (A, B, C)
	for (var i = 0; i < COLUMNS; i++) {
		var th = document.createElement("th");
		th.innerHTML = String.fromCharCode(65 + i);
		header.appendChild(th);
	}
	thead.appendChild(header);
	table.appendChild(thead);

	// make body stuff
	var tbody = document.createElement("tbody");

	var ind = 0; // index to iterate through the SEATS array

	// fill table body rows and columns
	for (var i = 0; i < ROWS; i++) {
		var tr = document.createElement("tr");

		var rowNr = document.createElement("td");
		rowNr.innerHTML = (i + 1);

		tr.appendChild(rowNr);

		for (var j = 0; j < COLUMNS; j++) {
			var td = document.createElement("td");
			td.appendChild(seats[ind++]);
			tr.appendChild(td);
		}
		tbody.appendChild(tr);
	}
	table.appendChild(tbody);
	document.getElementById("seatTable").appendChild(table);
}

/**
 * Load session storage
 * When the seat table is populated, load the seat number and color in the
 * session storage for tracking.
 * sessionStorage key/value pair ==> img.alt: img.style.backgroundColor
 */
function loadSessionStorage()
{
	var loadedSeats = document.getElementsByClassName("seat");

	for (var j = 0; j < loadedSeats.length; j++) {
		var seatImg = loadedSeats[j];
		var seatColor = seatImg.style.backgroundColor;

		sessionStorage.setItem(seatImg.alt, seatColor);
	}
}

/**
 * Change the background color of a particular seat(img object).
 * @param seat image whose background is to be changed
 * @param color background color to set
 */
function setSeatBackground(seat, color)
{
	seat.style.backgroundColor = color;
}

/**
 * sessionStorage has the current seat colors.
 * When reloading a new page, let the seats(img) assume the color that
 * is stored in the session storage
 */
function updateSeatColor()
{
	if (window.sessionStorage.length !== 0) {
		for (var i = 0; i < seats.length; i++) {

			var key = window.sessionStorage.key(i);
			var color = window.sessionStorage.getItem(key);
			var seat = seats[i];
			if (key === seat.alt) {
				setSeatBackground(seat, color);
			}
		}
	}
}

/**
 * Change a seat background from green to blue. This will happen when a seat
 * is chosen.
 * @param seat seat whose background is to be changed
 */
function changeSeatColor(seat)
{
	if (seat.style.backgroundColor === "green") {
		seat.style.backgroundColor = "blue";
	}
}

/**
 * Seat seat number when user clicks on the corresponding seat on the table
 * @param seat the clicked seat(img)
 */
function setSeatNr(seat)
{
	document.getElementById("seatNr").value = seat.alt;
}

/**
 * Set the flight class value when a user clicks on the corresponding seat
 * @param seat the clicked seat(img)
 */
function setFlightClass(seat)
{
	document.getElementById("status").value = seat.title.toUpperCase();
}


/**
 * When the user clicks on a seat, the following occurs
 * 1. the color changes from green to blue
 * 2. the seat number field is updated
 * 3. the flight class field is updated
 * 3. the form data is set
 * 4. sessionStorage updates with the new colors
 * 5. colors from session storage are sent to the flight seats so that they can
 * update appropriately when next window upload occurs
 *
 * */
function chooseSeat()
{

	var allSeats = document.getElementsByClassName("seat");
	for (var j = 0; j < allSeats.length; j++) {
		var seat = allSeats[j];

		var event = "click";

		if (seat.style.backgroundColor === "green") {
			seat.addEventListener(event, function () {
				changeSeatColor(this);
			}, false);

			seat.addEventListener(event, function () {
				setSeatNr(this);
			}, false);

			seat.addEventListener(event, function () {
				setFlightClass(this);
			}, false);
		}
		seat.addEventListener(event, setFormData, false);
		seat.addEventListener(event, resetSelected, false);
		seat.addEventListener(event, loadSessionStorage, false);
		seat.addEventListener(event, updateSeatColor, false);
		//seat.addEventListener(event, showStorage, false);
		//seat.addEventListener(event, availableSeats, false);
	}
}


/**
 * Get the number of seats available
 * Display seat numbers for economy and business seats that are available as a
 * hint to the user at selection
 */
// function availableSeats()
// {
//
// 	var eSeats = [];
// 	var bSeats = [];
// 	var occupiedSeat = 0;
// 	var allLoadedSeats = document.getElementsByClassName("seat");
//
// 	for (var i = 0; i < allLoadedSeats.length; i++) {
// 		var seat = allLoadedSeats[i];
// 		if (seat.style.backgroundColor === "green") {
// 			if (seat.title.toLowerCase() === "economy") {
// 				eSeats.push(seat.alt);
// 			} else {
// 				bSeats.push(seat.alt);
// 			}
// 		}
// 		if (seat.style.backgroundColor === "blue") {
// 			++occupiedSeat;
// 		}
// 	}
// 	// show available seat data
// 	var parent = document.getElementById("seatTable");
// 	var p1 = document.createElement("p");
// 	var p2 = document.createElement("p");
// 	var p3 = document.createElement("p");
// 	p1.class = "available";
// 	p2.class = "available";
// 	p3.class = "available";
//
// 	p1.innerHTML = "Available economy seats [" + eSeats + "]";
// 	p2.innerHTML = "Available business seats [" + bSeats + "] ";
// 	p3.innerHTML = "Occupied seats : [" + occupiedSeat + "]";
//
// 	// parent.appendChild(p2);
// 	// parent.appendChild(p1);
// 	// parent.appendChild(p3);
// 	if (occupiedSeat < 1) {
// 		p3.style.display = "none";
// 	}
// }

/**
 * extract the data from the form for the boarding pass
 */
function setFormData()
{
	var form = document.booking_form;
	firstName = form.fname.value;

	lastName = form.lname.value;

	status = form.status.value;
	if(status === ""){
		status = "no seat, hence no class";
	}

	seatNr = form.seatNr.value;
	if(seatNr === ""){
		seatNr = "no seat chosen. You will get a random seat";
	}
	var dep = document.getElementById("departure");
	departure = dep.options[dep.selectedIndex].text;

	var arr = document.getElementById("destination");
	destination = arr.options[arr.selectedIndex].text;

	depDate = form.departure_date.value;
	arrDate = form.arrival_date.value;
}


/**
 * Make a print out of the booking pass
 */
function generateBoardingPass()
{
	var today = new Date();

	var page = window.open("");
	page.document.writeln("<!doctype html><html id='bdpass'>");
	page.document.writeln(" <head><meta charset='UTF-8'><title>"
		+ "Boarding pass for " + lastName + " generated on the " + today
		+ "</title><link href='css/style.css' rel='stylesheet'></head>");
	page.document.writeln("<body class='boardingPass'><h1>Boarding pass" +
		" for " + lastName + "</h1>");
	page.document.write("<table id='boardingPass'><tbody><tr><td>"
		+ "Full Name</td><td>" + lastName + " " + firstName +
		"</td></tr>");
	page.document.write("<tr><td>Seat nr </td><td>" + seatNr +
		"</td></tr>");
	page.document.write("<tr><td>Class</td><td>" + status + "</td></tr>");
	page.document.write("<tr><td>From</td><td>" + departure +
		"</td></tr>");
	page.document.write("<tr><td>To</td><td>" + destination +
		"</td></tr>");
	page.document.write("<tr><td>Departure date</td><td>" + depDate +
		"</td></tr>");
	page.document.write("<tr><td>Arrival date</td><td>" + arrDate +
		"</td></tr>");
	page.document.write("</tbody></table></body></html>");

}

/**
 * Make booked seat "unchoosable" for the next session by changing the color
 * from blue to red. This makes the seat "unrecognizable" by
 * <code>changeSeatColor</code>
 * In case the user had clicked more than one seat, change the remainder
 * seats with background blue to green so that they are available for next
 * session.
 */
function deactivateBookedSeats()
{
	var allSeats = document.getElementsByClassName("seat");
	for (var j = 0; j < allSeats.length; j++) {
		if (allSeats[j].style.backgroundColor === "blue"
			&& allSeats[j].alt === seatNr) { // this
			setSeatBackground(allSeats[j], "red");
			loadSessionStorage();
			updateSeatColor();
		}
	}
}

/**
 * Until a user enters a valid personal number, do no allow seat selection
 */
function enableSeatSelection(){
	document.getElementById("personalNumber").addEventListener(
			"blur", chooseSeat, false);
}
/**
 * If user clicks on another unbooked/selected seat, change the other blue
 * seat to green so that we have just one blue seat at a time in the seat table
 */
function resetSelected()
{
	var allSeats = document.getElementsByClassName("seat");
	for (var j = 0; j < allSeats.length; j++) {
		if (allSeats[j].style.backgroundColor === "blue"
			&& allSeats[j].alt !== seatNr) {
			setSeatBackground(allSeats[j], "green");
			loadSessionStorage();
			updateSeatColor();
		}
	}
}

/**
 * When form is submitted,
 *  - generate boarding pass
 *  - make selected seats unavailable for selection for the very next
 * booking
 * - reset the form fields
 */
function submitForm()
{
	var event = "submit";
	var form = document.booking_form;
	//form.addEventListener(event, setFormData, false);
	form.addEventListener(event, generateBoardingPass, false);
	form.addEventListener(event, deactivateBookedSeats, false);
	document.getElementById("booking_form").reset();
}


/**
 * Debugging storage to see that things are working as they should
 */
function showStorage()
{
	console.clear();
	var ind = 0;
	while (ind < sessionStorage.length) {
		console.log(sessionStorage.key(ind) + " "
			+ sessionStorage.getItem(sessionStorage.key(ind)));
		ind++;
	}
}

/**
 * Initialize the program when page loads
 */
function init()
{
	loadSeats();
	loadSessionStorage();
	updateSeatColor();
	showSeatTable();
}

window.addEventListener("load", init, false);
window.addEventListener("load", enableSeatSelection, false);
window.addEventListener("load", submitForm, false);