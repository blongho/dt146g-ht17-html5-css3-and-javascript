/*******************************************************************************
 * Laboration 2, Kurs: DT146G
 * File: main.js
 * Desc: main JavaScript file for laboration 2
 *
 * Bernard Che Longho
 * lobe1602
 * lobe1602@student.miun.se
 ******************************************************************************/

/*
    Get the browser
 */
function getBrowser() {
    var sBrowser, sUsrAg = navigator.userAgent;

    if (sUsrAg.indexOf("Chrome") > -1) {
        sBrowser = "Google Chrome";
    } else if (sUsrAg.indexOf("Safari") > -1) {
        sBrowser = "Apple Safari";
    } else if (sUsrAg.indexOf("Opera") > -1) {
        sBrowser = "Opera";
    } else if (sUsrAg.indexOf("Firefox") > -1) {
        sBrowser = "Mozilla Firefox";
    } else if (sUsrAg.indexOf("MSIE") > -1) {
        sBrowser = "Microsoft Internet Explorer";
    }
    return sBrowser;
}

/*
    Add the browser to its place.
    Color the background yellow so it stands out
 */
function addBrowser() {
    var browser = getBrowser();
    var para = document.getElementById("currentBrowser");
    para.textContent += browser;
    para.style.backgroundColor = "yellow";

}

/**
 * @brief Get current date plus or minus an addition month
 * @param addMonth
 * @returns {string}
 */
function currentDate(addMonth) {
    var today = new Date();
    var dd = today.getDate();
    today.setMonth(today.getMonth() + 1 + addMonth); //January is 0!

    var mm = today.getMonth();

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = "0" + dd;
    }
    if (mm < 10) {
        mm = "0" + mm;
    }
    return yyyy + "-" + mm + "-" + dd;
}


/**
 * @brief Set the min and max start dates in booking a flight
 * Min start  = today
 * max booking = 7 months from today
 */
function setDateIntervals() {
    // Set min and max values for departure
    var departure = document.getElementById("depDate");
    departure.setAttribute("min", currentDate(0));
    departure.setAttribute("max", currentDate(7));

    var arrival = document.getElementById("arrDate");
    arrival.setAttribute("min", currentDate(0));
    arrival.setAttribute("max", currentDate(7));
}

/**
 * @brief Let js set the current year in the footer
 */
function setFooterYear() {
    var yr = new Date();
    var footerYear = document.getElementById("currentYear");
    footerYear.textContent += yr.getFullYear();
}


/**
 * @brief Get the number of passengers
 * @returns {string|Number} The number of passengers as entered by user
 */
function numberOfPassengers() {
    return document.getElementById("passengers").value;
  }

/**
 * @brief If number of person is greater than one,
 *  repeat field for passenger name and sex
 *  append content to tell the passenger to input email and phone number of main passenger
 */

function addMoreFields() {
    var count = numberOfPassengers();

    if (count > 1 && count <= 7) {
        var node = document.getElementById("passenger_details");
        var main_passenger = document.getElementById("main_passenger");

        main_passenger.textContent += "Contact details of main passenger";

        var copy;
        for (var i = 0; i < (count - 1); i++) {
            copy = node.cloneNode(true);
            node.parentNode.insertBefore(copy, node);
        }
    }
}
