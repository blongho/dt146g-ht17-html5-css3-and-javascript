/*******************************************************************************
 * Laboration 3, Kurs: DT146G
 * File: main.js
 * Desc: main JavaScript file for laboration 3
 *
 * Bernard Che Longho
 * lobe1602
 * lobe1602@student.miun.se
 * 2017-12-07
 ******************************************************************************/

/**
 * Get the current file.
 * @returns {string} the current html page name
 */
function getCurrentPage()
{
	var path = window.location.pathname;
	var file = path.substr(path.lastIndexOf("/") + 1);
	return file;
}

/**
 * @type {string} Current page
 */
var currentPage = getCurrentPage();

/**
 * Known images
 * NB: Take note of the order in which these images are placed i.e their indices
 * @type {[string,string,string,string]} path to images to be loaded
 */
var images = [
	"img/manager.png",
	"img/marketing.png",
	"img/serviceHead.png",
	"img/humanResource.png"
];

/**
 * Big plane images
 * @type {[string,string,string,string]} path to images
 */
var planes = [
	"img/boeing734xg.jpg",
	"img/boeing787m2.jpg",
	"img/boeing8094.jpg",
	"img/boeing8096.jpg"
]
/** an array to hold the larger images */
var bigImages = [];

/** an array to hold the larger plane images */
var bigPlanes = [];

/** an array to hold the employee details */
var employeeDetails = [];

/**
 * An array to hold plane properties.
 * When a plane's thumbnail is clicked, a bigger image of the plane is
 * displayed to the right and a short description about the plane is displased.
 * Such descriptions are stored in this array.
 * @type {Array} Array to hold plane properties
 */
var planeProperties = [];

/**
 * Extract plane names once the page is loaded.
 * @type {Array}
 */
var planeNames = [];

/*
 Get the browser
 */
function getBrowser()
{
	var sBrowser, sUsrAg = navigator.userAgent;

	if (sUsrAg.indexOf("Chrome") > -1) {
		sBrowser = "Google Chrome";
	} else if (sUsrAg.indexOf("Safari") > -1) {
		sBrowser = "Apple Safari";
	} else if (sUsrAg.indexOf("Opera") > -1) {
		sBrowser = "Opera";
	} else if (sUsrAg.indexOf("Firefox") > -1) {
		sBrowser = "Mozilla Firefox";
	} else if (sUsrAg.indexOf("MSIE") > -1) {
		sBrowser = "Microsoft Internet Explorer";
	}
	return sBrowser;
}

/*
 Add the browser to its place.
 Color the background yellow so it stands out
 */
function addBrowser()
{
	var browser = getBrowser();
	var para = document.getElementById("currentBrowser");
	para.textContent += browser;
	para.style.backgroundColor = "yellow";

}

/**
 * @brief Get current date plus or minus an addition month
 * @param month Months to add
 * @returns {string}
 */
function currentDate(month)
{
	var today = new Date();
	var dd = today.getDate();

	var thisMonth = today.getMonth();

	today.setMonth(thisMonth + month);

	var mm = today.getMonth() +1;

	var yyyy = today.getFullYear();

	if (dd < 10) {
		dd = "0" + dd;
	}
	if (mm < 10) {
		mm = "0" + mm;
	}
	return yyyy + "-" + mm + "-" + dd;
}


/**
 * @brief Set the min and max start dates in booking a flight
 * Min start  = today
 * max booking = 7 months from today
 */
function setDateIntervals()
{
	// Set min and max values for departure
	var departure = document.getElementById("depDate");
	// departure.setAttribute("min", currentDate(0));
	// departure.setAttribute("max", currentDate(7));
	departure.min = currentDate(0);
	departure.max = currentDate(7);

	var arrival = document.getElementById("arrDate");
	//arrival.setAttribute("min", currentDate(0));
	//arrival.setAttribute("max", currentDate(7));
	arrival.min = currentDate(0);
	arrival.max = currentDate(7);
}

/**
 * @brief Let js set the current year in the footer
 */
function setFooterYear()
{
	var yr = new Date();
	var footerYear = document.getElementById("currentYear");
	footerYear.textContent += yr.getFullYear();
}


/**
 * @brief Get the number of passengers
 * @returns {string|Number} The number of passengers as entered by user
 */
function numberOfPassengers()
{
	return document.getElementById("passengers").value;
}

/**
 * @brief If number of person is greater than one,
 *  repeat field for passenger name and sex
 *  append content to tell the passenger to input email and phone number of
 *     main passenger
 */
function addMoreFields()
{
	var count = numberOfPassengers();

	if (count > 1 && count <= 7) {
		var node = document.getElementById("passenger_details");

		var copy;
		for (var i = 0; i < (count - 1); i++) {
			copy = node.cloneNode(true);
			node.parentNode.insertBefore(copy, node);
		}
	}
	var main_passenger = document.getElementById("main_passenger");
	main_passenger.innerHTML = "Contact details of main passenger";
}

/*=============================================================================
 * Interactions with pages
 * =============================================================================
 */
/**
 * Depending on the loaded page, the images are loaded
 *
 * An array to hold all the large images for display
 * @type {[string,string,string,string]}
 * Load big plane images.
 */
function loadImages()
{
	if (currentPage === "employee.html") {
		for (var i = 0; i < images.length; i++) {
			bigImages[i] = new Image();
			bigImages[i].src = images[i];
			bigImages[i].alt = images[i].substr(4);
		}
		//alert("I have loaded images. Big images = " + bigImages.length);
	}
	else if (currentPage === "ourfleet.html") {
		for (var i = 0; i < planes.length; i++) {
			bigPlanes[i] = new Image();
			bigPlanes[i].src = planes[i];
			bigPlanes[i].alt = planes[i].substr(4);
		}
		//alert(bigPlanes[3].alt);
		//alert("I have loaded big planes. Planes = " + bigPlanes.length);
	}
}

/**
 * gather and keep large images as well as the paragraphs ready for download
 * Display the employee details for the manager as default
 * Call this in functions that work on the employee pages
 **/
function init()
{
	loadDetails();
	loadImages();
	showEmployeeDetail("manager");
	//showPlaneProperties("plane1");
}

/**
 * Preload details of employee and plane properties
 * Function only loads when user is in the concerned page.
 */
function loadDetails()
{
	if (currentPage === "employee.html") {
		var detailsList = document.getElementsByClassName("empDetails");

		for (var i = 0; i < detailsList.length; i++) {
			employeeDetails[i] = detailsList[i].innerHTML;
		}
		//alert("I have loaded emplyeeDetails " + employeeDetails.length);
	}
	else if (currentPage === "ourfleet.html") {
		var planePropList = document.getElementsByClassName("plane_properties");

		for (var i = 0; i < planePropList.length; i++) {
			planeProperties[i] = planePropList[i].innerHTML;
		}
		//alert("I have loaded plane properties " + planeProperties.length);

		// get plane names
		var boeing = document.getElementsByClassName("planeName");
		for (var ind = 0; ind < boeing.length; ind++) {
			planeNames[ind] = boeing[ind].innerHTML;
		}
		//alert("loaded planeNames. Items = " + planeNames);

	}
}

/**
 * Get an image based on its index in the <code>bigImages</code> array.
 * @param idx Index of image in image array
 * @returns {HTMLElement} Image element with src and alt attributes
 */
function setImage(idx)
{
	var imgBox = document.getElementById("bigImage");
	var img = imgBox.children[1];
	img.src = bigImages[idx].src;
	img.alt = bigImages[idx].alt;
	return img;
}

function setBigPlane(idx)
{
	var imgBox = document.getElementById("bigPlane");
	var img = imgBox.children[1];
	img.src = bigPlanes[idx].src;
	img.alt = bigPlanes[idx].alt;
	return img;
}

/**
 * Get the index of the image based on the the section id
 * @param sectionID
 * @returns {number}
 */
function getIndex(sectionID)
{
	var idx = 0;
	switch (sectionID) {
		case "manager":
		case "plane1":
			idx = 0;
			break;
		case "marketing":
		case "plane2":
			idx = 1;
			break;
		case "serviceHead":
		case "plane3":
			idx = 2;
			break;
		case "humanResource":
		case "plane4":
			idx = 3;
			break;
		default:
			alert("Out of range index!");
			break;
	}
	return idx;
}

/**
 * Get the employee's title based on the section id
 * @param sectionID Section id for which the employee heads
 * @returns {string} The employee's title
 */
function employeeTitle(sectionID)
{
	var employeeTitle = "";
	switch (sectionID) {
		case "manager":
			employeeTitle = "manager";
			break;
		case "marketing":
			employeeTitle = "Marketing director";
			break;
		case "serviceHead":
			employeeTitle = "Head of Service i/c repairs";
			break;
		case "humanResource":
			employeeTitle = "Human resource head";
			break;
		default:
			employeeTitle = "manager";
	}
	return employeeTitle;
}

/**
 * Get the big image using the sectionID
 * When displaying the larger image, display the header so the user knows what
 * he/she is viewing at the moment
 * @param sectionID section whose bigger image is to be displayed.
 */
function showBigImage(sectionID)
{
	var index = getIndex(sectionID);
	var imgParent = document.getElementById("bigImage");
	imgParent.firstElementChild.innerHTML =
		"Our " + employeeTitle(sectionID);
	imgParent.children[1] = setImage(index);

}

/**
 * Extract and show the bigger version of a plane using the section id
 * @param sectionID section from which the plane is located
 */
function showBigPlane(sectionID)
{
	var imgParent = document.getElementById("bigPlane");
	imgParent.children[1] = setBigPlane(getIndex(sectionID));
}

/**
 * When user clicks on an image, the portfolio of the person is displayed.
 * @param sectionID the id of the section whose info is to be updated
 */
function showEmployeeDetail(sectionID)
{
	var desc = document.getElementById("imgDescription");
	desc.children[0].innerHTML = "Our " + employeeTitle(sectionID)
		+ "'s portfolio";
	desc.children[1].innerHTML = employeeDetails[getIndex(sectionID)];
}

/**
 * When an image is clicked, its properties are displayed.
 * @param sectionID section id where the image is placed
 */
function showPlaneProperties(sectionID)
{
	// display title of image
	var title = document.getElementById("bigPlane");
	title.firstElementChild.innerHTML = planeNames[getIndex(sectionID)];

	// get and display summary about clicked plane
	var propNode = document.getElementById("plane-property-container");

	propNode.firstElementChild.innerHTML = "Summary of "
		+ planeNames[getIndex(sectionID)];

	// Do not append any other section
	var nodeToRemove = document.getElementById("planeProperties");
	var hasSection = propNode.querySelector("#planeProperties") !== null;

	if (hasSection) {
		propNode.removeChild(nodeToRemove);
	} else {
		var section = document.createElement("section");
		propNode.appendChild(section);
		section.setAttribute("id", "planeProperties");
		section.innerHTML = planeProperties[getIndex(sectionID)];
	}
}

/**
 * By clicking on the smaller images,
 * - the bigger image is displayed
 * - the text describing the image is also displayed.
 */
function changeImages()
{
	document.getElementById("mgr").addEventListener(
		"click", function () {
			showBigImage("manager")
		});
	document.getElementById("mkt").addEventListener(
		"click", function () {
			showBigImage("marketing")
		});
	document.getElementById("srv").addEventListener(
		"click", function () {
			showBigImage("serviceHead")
		});
	document.getElementById("hum").addEventListener(
		"click", function () {
			showBigImage("humanResource")
		});

	// listeners for details
	document.getElementById("mgr").addEventListener(
		"click", function () {
			showEmployeeDetail("manager")
		});
	document.getElementById("mkt").addEventListener(
		"click", function () {
			showEmployeeDetail("marketing")
		});
	document.getElementById("srv").addEventListener(
		"click", function () {
			showEmployeeDetail("serviceHead")
		});
	document.getElementById("hum").addEventListener(
		"click", function () {
			showEmployeeDetail("humanResource")
		});

}

/**
 * On clicking an image
 * - a bigger version of the plane is displayed
 * - a small descriptive text about the plane is displayed.
 */
function changePlanes()
{
	document.getElementById("boeing734xg")
		.addEventListener("click", function () {
			showBigPlane("plane1")
		});
	document.getElementById("boeing787m2")
		.addEventListener("click", function () {
			showBigPlane("plane2")
		});
	document.getElementById("boeing8094")
		.addEventListener("click", function () {
			showBigPlane("plane3")
		});
	document.getElementById("boeing8096")
		.addEventListener("click", function () {
			showBigPlane("plane4")
		});

	// listeners for details
	document.getElementById("boeing734xg")
		.addEventListener("click", function () {
			showPlaneProperties("plane1")
		});
	document.getElementById("boeing787m2")
		.addEventListener("click", function () {
			showPlaneProperties("plane2")
		});
	document.getElementById("boeing8094")
		.addEventListener("click", function () {
			showPlaneProperties("plane3")
		});
	document.getElementById("boeing8096")
		.addEventListener("click", function () {
			showPlaneProperties("plane4")
		});
}


// event listeners for fleet and employee pages only
if (currentPage === "ourfleet.html" || currentPage === "employee.html") {
	window.addEventListener("load", init, false);
	window.addEventListener("load", changeImages, false);
	window.addEventListener("load", changePlanes, false);
}

// on the contacts page, show the browser information
if (currentPage === "contact.html") {
	window.addEventListener("load", addBrowser, false);
}
// event listeners for the booking page
// sets date intervals and increase passenger fields if user deem fit

if (currentPage === "booking.html") {
	window.addEventListener("load", setDateIntervals, false);
	document.getElementById("passengers").addEventListener(
		"change", addMoreFields, false);
	//alert(currentDate(0));
}
// event listener for all pages
window.addEventListener("load", setFooterYear, false);



