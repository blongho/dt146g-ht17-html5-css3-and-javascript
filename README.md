# DT146G HT17 Web Programming with HTML5, CSS3 and JavaScript #

### What is this repository for? ###

This repository is for laboratory exercises for the course `dt146g : Webbprogrammering med HTML5, CSS3 och JavaScript`

* Version 1.0 


* [Course description](https://www.miun.se/utbildning/kurser/data-och-it/datateknik/datateknik-gr-a-webbprogrammering--med-html5-css3-och-javascript-75-hp/kursplan/)

### Live demo
1. Go to [http://studenter.miun.se/~lobe1602/dt146g/](http://studenter.miun.se/~lobe1602/dt146g/)
2. Click on Laboration [x] to see the progress of the labs

**NB** The labs are "primitive". I am particularly impressed with my project. Click on it and see how it works. 

**The project is in a separate repository with more details about it [click here to go there>>](https://bitbucket.org/blongho/dt146g_project/src/master/)


### Who do I talk to? ###

* Repo owner or admin

  - Bernard Longho [lobe1602@student.miun.se](mailto:lobe1602@student.miun.se)